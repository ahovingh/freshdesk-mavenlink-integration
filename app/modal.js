/* eslint-disable no-undef */
app.initialized()
  .then((_client) => {
    _client.data.get('ticket').then((data) => {
      initApp(data, _client);
    });
  });
