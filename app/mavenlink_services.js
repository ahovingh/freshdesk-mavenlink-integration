// eslint-disable-next-line no-unused-vars
class MLServices {

  constructor(client) {
    this.MLURL = 'https://api.mavenlink.com/api/v1/';
    this.client = client;
    this.headers = { Authorization: 'Bearer <%= access_token %>', 'Content-Type': 'application/json' };
  }


  createMultiplePosts(posts) {
    const postsData = { posts };
    return this.sendRequest(`${this.MLURL}posts.json`, 'post', postsData);
  }


  createTask(story) {
    return this.sendRequest(`${this.MLURL}stories.json`, 'post', story);
  }


  getProjects() {
    return this.sendRequest(`${this.MLURL}workspaces.json?per_page=200`, 'get');
  }


  getTasks(projectid) {
    return this.sendRequest(`${this.MLURL}stories.json?parents_only=true&uncompleted=true&workspace_id=${projectid}`, 'get');
  }


  getUsers() {
    return this.sendRequest(`${this.MLURL}users.json?on_my_account=true`, 'get');
  }


  // object is an optional parameter
  sendRequest(apiURL, method, object) {
    let req;
    const options = {
      headers: this.headers,
      isOAuth: true,
    };

    // Only get and post is supported right now
    if (object && method === 'post') {
      options.body = JSON.stringify(object);
      req = this.client.request.post(apiURL, options);
    } else {
      req = this.client.request.get(apiURL, options);
    }

    return req.then(data => JSON.parse(data.response));
  }
}
