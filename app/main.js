// eslint-disable-next-line no-unused-vars
const initApp = (ticketInfo, client) => {
  /* eslint-disable no-undef */
  const fdService = new FDServices(client);
  const mlService = new MLServices(client);

  console.log('TICKET:', ticketInfo);
  // eslint-disable-next-line no-unused-vars
  const app = new Vue({
    el: '#FDExportContainer',


    data: {
      importedStory: {},
      includeConversations: true,
      errorMessage: '',
      messageIsSending: false,
      messageIsSuccessful: false,
      mlProjects: [],
      mlTasks: [],
      selectedProject: {},
      selectedTask: {},
      taskDescription: ticketInfo.ticket.description_text,
      taskDueDate: new Date(),
      taskEstimate: 0,
      taskTitle: ticketInfo.ticket.subject,
    },


    mounted() {
      try {
        this.getProjects();
      } catch (appErr) {
        app.errorMessage = appErr;
      }
    },

    watch: {
      errorMessage() {
        this.messageIsSending = false;
        this.messageIsSuccessful = false;
      },
    },


    computed: {
      mainContainerShow() {
        return !this.messageIsSending && !this.messageIsSuccessful && !this.errorMessage;
      },


      mlStory() {
        return {
          title: app.mlStoryTitle,
          story_type: 'task',
          workspace_id: app.selectedProject.id,
          due_date: app.taskDueDate,
          description: app.taskDescription,
          parent_id: app.selectedTask.id,
          assignee_ids: [],
        };
      },


      mlStoryTitle() {
        let taskName = app.taskTitle;

        if (taskName === '') {
          taskName = ticketInfo.ticket.subject;
        }

        if (app.taskEstimate > 0) {
          taskName = `[${app.taskEstimate}] ${taskName}`;
        }

        return taskName;
      },
    },


    methods: {
      exportTicket() {
        bus.$emit('validate_all');

        // Passing in the object instance, name of the function, and the parameters we are giong to pass to the request function
        const requestData = (reqObj, reqObjFunction, ...params) => {
          reqObjFunction.apply(reqObj, params).then((data) => {
            // eslint-disable-next-line no-use-before-define
            reqGen.next(data);
          }).catch((appErr) => {
            app.errorMessage = 'There was a problem exporting the task. Contact Albert if you get this error';
            console.log(reqObjFunction);
            console.log(params);
            console.error(appErr);
          });
        };

        function* exportSteps() {
          if (app.selectedTask.id && app.selectedProject.id) {
            app.messageIsSending = true;
            console.log('mavenlinkStory', app.mlStory);
            console.log('mavenlinkStoryTitle', app.mlStoryTitle);
            console.log('estimate', app.taskEstimate);
            const taskData = yield requestData(mlService, mlService.createTask, app.mlStory);
            app.importedStory = taskData.stories[taskData.results[0].id];

            if (app.includeConversations) {
              const conversations = yield requestData(fdService, fdService.getConversations, ticketInfo.ticket.id);
              // TODO: If we have a private conversation we need to distinguish between private and public, not just a catch-all
              let userIDs = [];

              if (conversations.some(e => e.private)) {
                const uData = yield requestData(mlService, mlService.getUsers);
                userIDs = uData.results.map(a => a.id);
              }

              const posts = [];

              for (const conversation of conversations) {
                let tUserIDs = userIDs;
                if (!conversation.private) {
                  tUserIDs = [];
                }
                const post = FDHelper.buildPost(conversation, tUserIDs, app.importedStory);
                posts.push(post);
              }

              yield requestData(mlService, mlService.createMultiplePosts, posts);
            }

            const successData = yield requestData(client.db, client.db.set, `contactID:${ticketInfo.ticket.requester_id}`, { mavenlinkProjectID: app.selectedProject.id });

            if (successData.Created) {
              app.messageIsSending = false;
              app.messageIsSuccessful = true;
            } else {
              app.errorMessage = 'The task was successfully imported, but there was a problem saving some of your preferences. Contact Albert if you continue to receive this error';
              console.log(`Data confirmation message: ${JSON.stringify(successData)}`);
            }
          }
        }

        const reqGen = exportSteps();
        reqGen.next();
        app.messageIsSending = false;
      },


      getProjects() {
        mlService.getProjects()
          .then((projectIDObjects) => {

            app.mlProjects = FDHelper.getMLObjArray(projectIDObjects.workspaces, 'id').sort(FDHelper.sortByObjectTitle);

            client.db.get(`contactID:${ticketInfo.ticket.requester_id}`).then((data) => {
              console.log('Saved Data: ', data);
              [app.selectedProject] = app.mlProjects.filter(proj => proj.id === data.mavenlinkProjectID);
            }).catch(err => console.error(err));
          })
          .catch((err) => {
            app.errorMessage = err.message;
          });
      },


      getTasks(selectedProject) {
        this.selectedProject = selectedProject;

        mlService.getTasks(selectedProject.id)
          .then((data) => {
            app.mlTasks = FDHelper.getMLObjArray(data.stories, 'id').sort(FDHelper.sortByObjectTitle);
          })
          .catch((err) => {
            app.errorMessage = err.message;
          });
      },
    },
  });
};

