// eslint-disable-next-line
const bus = new Vue({});

// eslint-disable-next-line
Vue.component('dropdown', {

  template: `
  <div class="dropdown">
    <button class="btn btn-default dropdown-toggle" :class="{'shakethis': shakeThis}" type="button" :id="'dropdownMenu' + listName" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
      <span :value="selectedvalue">{{selectedtext}}</span>
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu" :aria-labelledby="'dropdownMenu' + listName">
      <li v-for="item in items" @click="selectOption(item)"><a class="dropdown-item" href="#">{{item.title}}</a></li>
    </ul>
  </div>`,


  props: {

    beforeItemsLoad: String,
    items: {
      type: Array,
    },
    listName: String,
    defaultItem: Object,
  },


  data() {
    return {
      selectedItem: {},
      defaultedString: `Select ${this.listName}`,
      shakeThis: false,
    };
  },


  // Pass the option back up to the parent when we select it in case there is soemthing depending on us
  methods: {
    selectOption(optionSelected) {
      this.shakeThis = false;
      this.selectedItem = optionSelected;
      this.$emit('optionselected', optionSelected);
    },
  },


  mounted() {
    bus.$on('validate_all', () => {
      if (this.selectedtext === this.defaultedString || this.selectedtext === this.beforeItemsLoad) {
        this.shakeThis = true;
      }
    });
  },


  // We want to let the user know when it's loading, and when it's a default value
  computed: {

    selectedtext() {
      if (this.items.length > 0) {
        // If we've selected an item, then display it
        if (!(Object.keys(this.selectedItem).length === 0 && this.selectedItem.constructor === Object)) {
          return this.selectedItem.title;
        }
        // If we've passed in a default item, then select it
        if (!(Object.keys(this.defaultItem).length === 0 && this.defaultItem.constructor === Object)) {
          this.selectOption(this.defaultItem);
          return this.defaultItem.title;
        }
        return this.defaultedString;
      }
      return this.beforeItemsLoad;
    },


    selectedvalue() {

      if (this.selectedItem.length > 0) {
        return this.selectedItem.id;
      }

      if (!(Object.keys(this.defaultItem).length === 0 && this.defaultItem.constructor === Object)) {
        return this.defaultItem.id;
      }
      return 'default';
    },

  },


  // If our select list changes, we don't want to keep the same value, just put it back to the default
  watch: {
    items() { this.selectedItem = {}; },
  },

});

// eslint-disable-next-line
Vue.component('sending-message', {
  template: `
  <div class="sendingcontainer">
    <div class="box">
        <div class="border one"></div>
        <div class="border two"></div>
        <div class="border three"></div>
        <div class="border four"></div>

        <div class="line one"></div>
        <div class="line two"></div>
        <div class="line three"></div>
    </div>
  </div>`,
});

// eslint-disable-next-line
Vue.component('success-message', {
  template: `
  <div>
    <div class="check_mark">
      <div class="sa-icon sa-success animate">
        <span class="sa-line sa-tip animateSuccessTip"></span>
        <span class="sa-line sa-long animateSuccessLong"></span>
        <div class="sa-placeholder"></div>
        <div class="sa-fix"></div>
      </div>
    </div>
    <div class="sa-message">&nbsp;&nbsp;Success!</div>
    <div class="sa-subtitle">&nbsp;&nbsp;Ummm....you can close this out now...</div>
  </div>`,
});

// eslint-disable-next-line
Vue.component('error-message', {
  template: `
  <div>
    <div class="check_mark">
      <div class="sa-icon sa-error animate">
        <span class="sa-line sa-tip animateErrorTip"></span>
        <span class="sa-line sa-long animateErrorLong"></span>
        <div class="sa-placeholder"></div>
        <div class="sa-fix"></div>
      </div>
    </div>
    <div class="sa-message">&nbsp;&nbsp;Error!</div>
    <br />
    <p>{{exceptionMessage}}</p>
  </div>`,
  props: ['exceptionMessage'],
});
