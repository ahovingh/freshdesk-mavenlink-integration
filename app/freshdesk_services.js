// TODO: Make settings for FD API URL and Auth Key for FD API URL
// eslint-disable-next-line no-unused-vars
class FDServices {
  constructor(client) {
    client.data.get('domainName').then((data) => {
      this.freshdeskDomain = `https://${data.domainName}`;
    });

    client.iparams.get().then((iparams) => {
      const authKey = `${iparams.freshdesk_api_key}:X`;
      this.headers = { Authorization: `Basic ${btoa(authKey)}`, 'Content-Type': 'application/json' };
    });
    this.client = client;
  }


  getConversations(ticketID) {
    const options = {
      headers: this.headers,
      isOAuth: true,
    };
    return this.client.request.get(`${this.freshdeskDomain}/api/v2/tickets/${ticketID}/conversations`, options)
      .then(data => JSON.parse(data.response));
  }
}

