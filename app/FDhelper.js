// eslint-disable-next-line no-unused-vars
class FDHelper {

  static buildPost(conversation, userIDs, MLTask) {
    const message = `${FDHelper.formatDateString(conversation.created_at)}\n${conversation.body_text}`;
    let recipients = [];

    if (conversation.private) {
      recipients = userIDs;
    }
    const post = {
      story_id: MLTask.id,
      workspace_id: MLTask.workspace_id,
      message,
    };

    if (recipients.length > 0) {
      post.recipient_ids = recipients.join();
    }

    return post;
  }


  static formatDateString(dateString) {
    const date = new Date(dateString);

    const monthNames = [
      'January', 'February', 'March',
      'April', 'May', 'June', 'July',
      'August', 'September', 'October',
      'November', 'December',
    ];

    return `${monthNames[date.getMonth()]} ${date.getDate()} ${date.getFullYear()}`;
  }


  static getMLObjArray(IDObjects, idName) {
    const objArray = [];
    for (const property in IDObjects) {
      if (IDObjects.hasOwnProperty(property)) {
        const pushObj = IDObjects[property];
        pushObj.id = IDObjects.constructor.name;
        pushObj[idName] = property;
        objArray.push(pushObj);
      }
    }
    return objArray;
  }


  static sortByObjectTitle(a, b) {
    if (a.title < b.title) {
      return -1;
    }
    return 1;
  }


}
