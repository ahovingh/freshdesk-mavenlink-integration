/* eslint-disable */
app.initialized()
   .then((_client) => {
     _client.events.on('app.activated', () => {
          _client.interface.trigger('showModal', {
            title: 'Export to Mavenlink',
            template: 'modal.html',
          });
      });
  });
